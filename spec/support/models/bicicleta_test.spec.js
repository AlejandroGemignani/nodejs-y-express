var mongoose = require('mongoose');
var Bicicleta = require('../../../models/bicicleta');

describe('Testeando bicicletas', function(){
    beforeAll(function(done){
        mongoose.connection.close().then(() =>{
            var mongoDB =  'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error'));
            db.once('open', function(){
                console.log('Ahora estamos conectados a la base de datos de prueba.');
                done();
            });    
        }); 

    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', () =>{
            var bici = Bicicleta.createInstance(1, "rojo", "bmx", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("rojo");
            expect(bici.modelo).toBe("bmx");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        
        });
    });

    describe('Bicicleta.allBicis', () =>{
        it('comienza vacía', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () =>{
        it('Agrega una bicicleta', (done) =>{
            var aBici = new Bicicleta({code: 1, color: "negro", modelo: "bmx"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });  
        });
    });

    describe('Bicicleta.findByCode', ()=>{
        it('Debe devolver una bici con code 1', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
            
                var aBici = new Bicicleta({code: 1, color:"negro", modelo: "bmx"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);
                    
                    var aBici2 = new Bicicleta({code: 2, color:"verde", modelo: "playa"});
                    Bicicleta.add(aBici, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1, function(error, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        })
                    })
                });
            });
        });
    });

    

});