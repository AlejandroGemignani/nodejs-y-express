var mongoose = require('mongoose');
var Bicicleta = require('../../../models/bicicleta');
var Usuario = require('../../../models/usuario');
var Reserva = require('../../../models/reserva');

describe('Testeando Usuarios', function(){
    
    beforeAll(function(done){
        mongoose.connection.close().then(() =>{
            var mongoDB =  'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error'));
            db.once('open', function(){
                console.log('Ahora estamos conectados a la base de datos de prueba.');
                done();
            });    
        }); 

    });

    afterEach(function(done){
        Reserva.deleteMany({},function(err, success){
            if(err)console.log(err);
            Usuario.deleteMany({},function(err, success){
                if(err)console.log(err);
                Bicicleta.deleteMany({},function(err, success){
                    if(err)console.log(err);
                    done();
                });
            });
        });
    });

    describe('Cuando un Usuario reserva una bici', ()=>{
        it('Desde existir la reserva' , (done)=>{
            const usuario = new Usuario({nombre: "Alejandro", email: "test5@gmail.com", password: "123456"});
            usuario.save();
            console.log(usuario);
            const bicicleta = new Bicicleta({code: 1, color: "negro", modelo: "carrera", ubicacion: [-34,54]});
            bicicleta.save();
            console.log(bicicleta);

            var hoy = new Date();
            var mañana = new Date();
            
            mañana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta._id, hoy, mañana, function(err, reserva){
                //console.log(reserva);
                Reserva.find({}).populate({path: 'bicicleta', model: Bicicleta}).populate({path: 'usuario', model: Usuario}).exec(function (err, reservas){
                    if(err) throw err;  

                    console.log(reservas[0]);
                    
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });  
            });
        });
    });

});