var express = require("express");
var router = express.Router();
const usuariosController = require("../controllers/usuario");

router.get("/", usuariosController.list);
router.get("/create", usuariosController.create_get);
router.post("/create", usuariosController.create);
router.get("/:_id/update", usuariosController.update_get);
router.post("/:_id/update", usuariosController.update);
router.post("/:_id/delete", usuariosController.delete);

module.exports = router;